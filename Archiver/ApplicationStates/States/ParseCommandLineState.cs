﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archiver.CommandLineParser;

namespace Archiver.ApplicationStates.States
{
    public class ParseCommandLineState : IAppState
    {
        private IAppStateManager _appStateManager = null;

        public ParseCommandLineState (IAppStateManager appStateManager)
        {
            _appStateManager = appStateManager;
        }

        public void ExecuteState ()
        {
            try
            {
                ICommandLineParser commandLineParser = new CommandLineParser.CommandLineParser ();
                _appStateManager.Command = commandLineParser.Parse (_appStateManager.Arguments);
                _appStateManager.SwitchState (
                    _appStateManager.AvailableStates.First(state => state is ExecuteCommandState));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString ());
                _appStateManager.SwitchState (
                    _appStateManager.AvailableStates.First (state => state is ExceptionState));
            }
        }
    }
}
