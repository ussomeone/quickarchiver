﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Archiver.Application;

namespace Archiver.ApplicationStates.States
{
    public class ExecuteCommandState : IAppState
    {
        private IAppStateManager _appStateManager = null;

        public ExecuteCommandState (IAppStateManager appStateManager)
        {
            _appStateManager = appStateManager;
        }

        public void ExecuteState ()
        {
            try
            {
                IAppContextManager appContextManager = new AppContextManager ();
                appContextManager.ExecuteStrategy (_appStateManager.Command);
                Console.WriteLine ("Operation successful");
                _appStateManager.StopStateManager ();
                Environment.ExitCode = 0;
                return;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString ());
                _appStateManager.SwitchState (_appStateManager.AvailableStates.First(state => state is ExceptionState));
                return;
            }
        }
    }
}
