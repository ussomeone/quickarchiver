﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.Devices;

namespace Archiver.Archiver.Misc
{
    public class BlockSizeHelper
    {
        private readonly int _minBlockSize;
        private readonly int _maxBlockSize;
        private float _memoryUsageFactor;
        private float _memoryStructureUsageFactor;

        public BlockSizeHelper (int minBlockSize, int maxBlockSize, float memoryUsageFactor, float memoryStructureUsageFactor)
        {
            _minBlockSize = minBlockSize;
            _maxBlockSize = maxBlockSize;
            _memoryUsageFactor = memoryUsageFactor;
            _memoryStructureUsageFactor = memoryStructureUsageFactor;
        }

        public int GetAverageBlockSize (long fileSize)
        {
            _memoryUsageFactor = _memoryUsageFactor > 1 ? 1 : _memoryUsageFactor < 0 ? 0 : _memoryUsageFactor;
            ComputerInfo _computerInfo = new ComputerInfo ();
            double blockSizeDouble = 
                (_computerInfo.AvailablePhysicalMemory) / 
                ((double) Environment.ProcessorCount * _memoryStructureUsageFactor) *
                _memoryUsageFactor;

            int blockSize;
            try
            {
                blockSize = checked ((int) blockSizeDouble);
            }
            catch (OverflowException exception)
            {
                blockSize = _maxBlockSize;
            }

            if (fileSize / Environment.ProcessorCount < blockSize && fileSize > _minBlockSize)
            {
                blockSize = (int)(fileSize / Environment.ProcessorCount);
            }

            blockSize = blockSize > _maxBlockSize ?  _maxBlockSize : blockSize < _minBlockSize ? _minBlockSize : blockSize;

            return blockSize;
        }
    }
}
