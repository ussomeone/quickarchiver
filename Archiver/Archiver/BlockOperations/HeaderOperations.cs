﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.Archiver.BlockOperations
{
    public static class HeaderOperations
    {
        public static void Copy<T> (T header, out byte[] bytes) where T : struct
        {
            var size = Marshal.SizeOf (header);
            IntPtr buffer = Marshal.AllocCoTaskMem (size);
            Marshal.StructureToPtr (header, buffer, false);
            bytes = new byte[size];
            Marshal.Copy (buffer, bytes, 0, size);
            Marshal.FreeCoTaskMem (buffer);
        }

        public static void Copy<T> (byte[] bytes, out T header) where T : struct
        {
            header = new T ();
            var size = Marshal.SizeOf (header);
            IntPtr buffer = Marshal.AllocCoTaskMem (size);
            Marshal.Copy (bytes, 0, buffer, size);
            header = (T)Marshal.PtrToStructure (buffer, typeof (T));
            Marshal.FreeCoTaskMem (buffer);
        }
    }
}
