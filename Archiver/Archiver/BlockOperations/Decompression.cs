﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using Archiver.SimpleObjectPool;

namespace Archiver.Archiver.BlockOperations
{
    public class Decompression : IBlockOperation
    {
        private readonly float _memoryUsageFactor = 3.5f;

        private int _blockSize = 0;
        private IObjectPool<byte[]> _byteArrayPool = null; 
        private IObjectPool<MemoryStream> _memoryStreamPool = null;

        public long MemoryUsage
        {
            get
            {
                return (long) (_blockSize * MemoryUsageFactor + 0.5f);
            }
        }

        public float MemoryUsageFactor
        {
            get
            {
                return _memoryUsageFactor;
            }
        }

        public Decompression (IObjectPool<MemoryStream> memoryStreamPool, IDataContainer dataContainer)
        {
            _memoryStreamPool = memoryStreamPool;
        }

        public void Initialize(IDataContainer dataContainer)
        {
            GZipHeader header = new GZipHeader ();
            byte[] bytes = new byte[Marshal.SizeOf (header)];
            dataContainer.InputStream.Read (bytes, 0, bytes.Length);
            HeaderOperations.Copy (bytes, out header);
            _blockSize = (int) header.MaxBlockSize;
            _byteArrayPool = new ArrayObjectPool<byte> (_blockSize);
            Console.WriteLine (string.Format ("MaxBlockSize set to {0} MB", (float) _blockSize / FastArchiver.MegaByte));
        }

        public void Finalize(IDataContainer dataContainer)
        {
        }

        public void Release ()
        {
            _byteArrayPool.Release ();
        }

        public void ReadBlock (out byte[] bytes, out int readLength, Stream inputStream)
        {
            bytes = _byteArrayPool.Get ();
            byte[] blockSize = new byte[sizeof(long)];
            inputStream.Read (blockSize, 0, blockSize.Length);
            readLength = (int) BitConverter.ToInt64 (blockSize, 0);
            inputStream.Read (bytes, 0, readLength);
        }

        public void WriteBlock (BlockData blockData, Stream outputStream)
        {
            blockData.OutputStream.Seek (0, SeekOrigin.Begin);
            blockData.OutputStream.CopyTo (outputStream);
        }

        public void Execute (LinkedListNode<BlockData> operationNode)
        {
            LinkedListNode<BlockData> element = operationNode as LinkedListNode<BlockData>;
            var operationData = element.Value;
            MemoryStream memoryStream = _memoryStreamPool.Get ();
            memoryStream.Write (operationData.InputData, 0, operationData.ReadLength);
            memoryStream.Seek (0, SeekOrigin.Begin);
            _byteArrayPool.Put(operationData.InputData);
            operationData.InputData = null;
            using (GZipStream compressionStream = new GZipStream (memoryStream, operationData.CompressionMode, true))
            {
                compressionStream.CopyTo (operationData.OutputStream);
            }
            _memoryStreamPool.Put (memoryStream);
            operationData.OperationState = OperationState.Finished;
        }
    }
}
