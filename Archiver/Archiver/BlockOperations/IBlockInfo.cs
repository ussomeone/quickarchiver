﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.Archiver.BlockOperations
{
    public interface IBlockInfo
    {
        long MemoryUsage
        {
            get;
        }

        float MemoryUsageFactor
        {
            get;
        }
    }
}
