﻿using Archiver.CommandLineParser.Commands;

namespace Archiver.CommandLineParser
{

    public interface ICommandLineParser
    {
        ICommand ParseCommand(string[] args);
        ICommand Parse(string[] args);
        bool TryParse(string[] args, out ICommand command);
        ICommand HelpCommand();
    }
}
