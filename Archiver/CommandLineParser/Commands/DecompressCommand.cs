﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.CommandLineParser.Commands
{
    public class DecompressCommand : AbstractCommand, IParsedData<DecompressCommand.DecompressData?>
    {
        public struct DecompressData
        {
            public string InputPath;
            public string OutputPath;
        }

        private string _commandName = "decompress";
        private readonly int _argumentsAmount = 3;

        public DecompressData? ParsedData
        {
            get;
            private set;
        }

        public override string GetCommandName ()
        {
            return _commandName;
        }

        public override int GetArgumentsAmount ()
        {
            return _argumentsAmount;
        }

        public override void Parse (string[] args)
        {
            var data = new DecompressData();
            data.InputPath = args[1];
            data.OutputPath = args[2];
            ParsedData = data;
        }

        public override bool TryParse (string[] args)
        {
            try
            {
                Parse (args);
                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine (exception.Message);
                return false;
            }
        }
    }
}
