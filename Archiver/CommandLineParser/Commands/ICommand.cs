﻿namespace Archiver.CommandLineParser.Commands
{
    public interface ICommand
    {
        bool IsCommandNameMatch(string[] args);
        bool CheckArguments(string[] args);
        void Parse(string[] args);
        bool TryParse(string[] args);
    }
}
