﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.CommandLineParser.Commands
{
    public class HelpCommand : AbstractCommand
    {
        private readonly string _commandName = "help";
        private readonly int _argumentsAmount = 1;

        public override string GetCommandName ()
        {
            return _commandName;
        }

        public override int GetArgumentsAmount ()
        {
            return _argumentsAmount;
        }

        public override void Parse(string[] args)
        {
            return;
        }

        public override bool TryParse(string[] args)
        {
            return false;
        }

        public string HelpOperation ()
        {
            var stringBuilder = new StringBuilder ( 7 );
            stringBuilder.Append ( "\nHelp:" );
            stringBuilder.Append ( "\n\"Don't support spaces without quotes\"" );
            stringBuilder.Append ( "\nAvailable operations:" );
            stringBuilder.Append ( "\nCompress" );
            stringBuilder.Append ( "\nDecompress" );
            stringBuilder.Append ( "\nExample:" );
            stringBuilder.Append ( "\ncompress image.jpg image.archived" );
            stringBuilder.Append ( "\ndecompress image.archived image.jpg" );
            return stringBuilder.ToString ();
        }
    }
}
