﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Archiver.CommandLineParser.Commands
{
    public class CompressCommand : AbstractCommand, IParsedData<CompressCommand.CompressData?>
    {
        public struct CompressData
        {
            public string InputPath;
            public string OutputPath;
        }

        private readonly int _argumentsAmount = 3;
        private readonly string _commandName = "compress";

        public CompressData? ParsedData
        {
            get;
            private set;
        }

        public CompressCommand()
        {
            ParsedData = null;
        }

        public override string GetCommandName ()
        {
            return _commandName;
        }

        public override int GetArgumentsAmount ()
        {
            return _argumentsAmount;
        }


        public override bool CheckArguments (string[] args)
        {
            if (!IsCommandNameMatch (args) || args.Length != _argumentsAmount)
            {
                return false;
            }
            return true;
        }

        public override void Parse (string[] args)
        {
            var data = new CompressData();
            data.InputPath = args[1];
            data.OutputPath = args[2];
            ParsedData = data;
        }

        public override bool TryParse(string[] args)
        {
            try
            {
                Parse(args);
                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                return false;
            }
        }
    }
}
