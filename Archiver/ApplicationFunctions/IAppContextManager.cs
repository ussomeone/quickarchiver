﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Archiver.CommandLineParser.Commands;

namespace Archiver.Application
{
    public interface IAppContextManager
    {
        void ExecuteStrategy (ICommand command);
    }
}
