﻿using System;
using System.Collections.Generic;
using Archiver.Application.Contexts;
using Archiver.CommandLineParser.Commands;

namespace Archiver.Application
{
    public class AppContextManager : IAppContextManager
    {
        private readonly Dictionary<Type, IAppContext> _appContexts = new Dictionary<Type, IAppContext> ()
        {
            {
                typeof (HelpCommand), new HelpContext ()
            },
            {
                typeof (CompressCommand), new CompressContext ()
            },
            {
                typeof (DecompressCommand), new DecompressContext ()
            }
        };

        public void ExecuteStrategy (ICommand command)
        {
            if (!_appContexts.ContainsKey (command.GetType ()))
            {
                throw new NotImplementedException("Command was't implemented");
            }
            _appContexts[command.GetType ()].Execute (command);
        }
    }
}
