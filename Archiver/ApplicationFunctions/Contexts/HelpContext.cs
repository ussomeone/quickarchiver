﻿using System;
using Archiver.CommandLineParser.Commands;

namespace Archiver.Application.Contexts
{
    public class HelpContext : IAppContext
    {
        public void Execute (ICommand command)
        {
            var helpCommand = command as HelpCommand;
            if (helpCommand == null)
            {
                throw new ArgumentException();
            }
            Console.WriteLine (helpCommand.HelpOperation ());
        }
    }
}
