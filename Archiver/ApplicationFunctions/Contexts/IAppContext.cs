﻿using Archiver.CommandLineParser.Commands;

namespace Archiver.Application.Contexts
{
    public interface IAppContext
    {
        void Execute (ICommand command);
    }
}
