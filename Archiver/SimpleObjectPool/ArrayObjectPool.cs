﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Archiver.SimpleObjectPool
{
    public class ArrayObjectPool<ArrayType> : IObjectPool<ArrayType[]>
    {
        private Queue<ArrayType[]> _container = null;
        private int _arraySize = 0;

        public ArrayObjectPool (int arraySize)
        {
            _container = new Queue<ArrayType[]> ();
            _arraySize = arraySize;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public ArrayType[] Get ()
        {
            if (_container.Count != 0)
            {
                return _container.Dequeue ();
            }
            return new ArrayType[_arraySize];
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Put (ArrayType[] value)
        {
            _container.Enqueue (value);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Release ()
        {
            _container = null;
        }
    }
}
