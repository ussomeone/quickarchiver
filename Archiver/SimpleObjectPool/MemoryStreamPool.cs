﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace Archiver.SimpleObjectPool
{
    public class MemoryStreamPool : IObjectPool<MemoryStream>
    {
        private Queue<MemoryStream> _container = null;
        private int _arraySize = 0;

        public MemoryStreamPool ()
        {
            _container = new Queue<MemoryStream> ();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public MemoryStream Get ()
        {
            if (_container.Count != 0)
            {
                return _container.Dequeue ();
            }
            return new MemoryStream();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Put (MemoryStream value)
        {
            _container.Enqueue (value);
            value.SetLength (0);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Release ()
        {
            foreach (var memoryStream in _container)
            {
                memoryStream.Dispose ();
            }
            _container = null;
        }
    }
}
